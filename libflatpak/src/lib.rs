#![cfg_attr(feature = "dox", feature(doc_cfg))]

// Re-export the -sys bindings
pub use ffi;

macro_rules! assert_initialized_main_thread {
    () => {};
}

macro_rules! skip_assert_initialized {
    () => {};
}

#[allow(unused_imports)]
mod auto;
pub use auto::*;

pub use auto::functions;
pub use auto::functions::*;
pub mod prelude;

mod installation;
