use crate::{
    InstallFlags, Installation, InstalledRef, Instance, LaunchFlags, RefKind, UninstallFlags,
    UpdateFlags,
};
use glib::object::IsA;
use glib::translate::*;
use std::ptr;

pub trait InstallationExtManual {
    #[doc(alias = "flatpak_installation_update_appstream_sync")]
    fn update_appstream_sync(
        &self,
        remote_name: &str,
        arch: Option<&str>,
        cancellable: Option<&impl IsA<gio::Cancellable>>,
    ) -> Result<(), glib::Error>;

    #[doc(alias = "flatpak_installation_update_appstream_full_sync")]
    fn update_appstream_full_sync(
        &self,
        remote_name: &str,
        arch: Option<&str>,
        progress: Option<&mut dyn (FnMut(&str, u32, bool))>,
        cancellable: Option<&impl IsA<gio::Cancellable>>,
    ) -> Result<(), glib::Error>;

    #[cfg_attr(feature = "v1_7", deprecated = "Since 1.7")]
    #[doc(alias = "flatpak_installation_update_full")]
    fn update_full(
        &self,
        flags: UpdateFlags,
        kind: RefKind,
        name: &str,
        arch: Option<&str>,
        branch: Option<&str>,
        subpaths: &[&str],
        progress: Option<&mut dyn (FnMut(&str, u32, bool))>,
        cancellable: Option<&impl IsA<gio::Cancellable>>,
    ) -> Result<InstalledRef, glib::Error>;

    #[cfg_attr(feature = "v1_7", deprecated = "Since 1.7")]
    #[doc(alias = "flatpak_installation_uninstall")]
    fn uninstall(
        &self,
        kind: RefKind,
        name: &str,
        arch: Option<&str>,
        branch: Option<&str>,
        progress: Option<&mut dyn (FnMut(&str, u32, bool))>,
        cancellable: Option<&impl IsA<gio::Cancellable>>,
    ) -> Result<(), glib::Error>;

    #[cfg_attr(feature = "v1_7", deprecated = "Since 1.7")]
    #[doc(alias = "flatpak_installation_uninstall_full")]
    fn uninstall_full(
        &self,
        flags: UninstallFlags,
        kind: RefKind,
        name: &str,
        arch: Option<&str>,
        branch: Option<&str>,
        progress: Option<&mut dyn (FnMut(&str, u32, bool))>,
        cancellable: Option<&impl IsA<gio::Cancellable>>,
    ) -> Result<(), glib::Error>;

    #[cfg_attr(feature = "v1_7", deprecated = "Since 1.7")]
    #[doc(alias = "flatpak_installation_update")]
    fn update(
        &self,
        flags: UpdateFlags,
        kind: RefKind,
        name: &str,
        arch: Option<&str>,
        branch: Option<&str>,
        progress: Option<&mut dyn (FnMut(&str, u32, bool))>,
        cancellable: Option<&impl IsA<gio::Cancellable>>,
    ) -> Result<InstalledRef, glib::Error>;

    #[cfg_attr(feature = "v1_7", deprecated = "Since 1.7")]
    #[doc(alias = "flatpak_installation_install")]
    fn install(
        &self,
        remote_name: &str,
        kind: RefKind,
        name: &str,
        arch: Option<&str>,
        branch: Option<&str>,
        progress: Option<&mut dyn (FnMut(&str, u32, bool))>,
        cancellable: Option<&impl IsA<gio::Cancellable>>,
    ) -> Result<InstalledRef, glib::Error>;

    #[cfg_attr(feature = "v1_7", deprecated = "Since 1.7")]
    #[doc(alias = "flatpak_installation_install_bundle")]
    fn install_bundle(
        &self,
        file: &impl IsA<gio::File>,
        progress: Option<&mut dyn (FnMut(&str, u32, bool))>,
        cancellable: Option<&impl IsA<gio::Cancellable>>,
    ) -> Result<InstalledRef, glib::Error>;

    #[cfg_attr(feature = "v1_7", deprecated = "Since 1.7")]
    #[doc(alias = "flatpak_installation_install_full")]
    fn install_full(
        &self,
        flags: InstallFlags,
        remote_name: &str,
        kind: RefKind,
        name: &str,
        arch: Option<&str>,
        branch: Option<&str>,
        subpaths: &[&str],
        progress: Option<&mut dyn (FnMut(&str, u32, bool))>,
        cancellable: Option<&impl IsA<gio::Cancellable>>,
    ) -> Result<InstalledRef, glib::Error>;

    #[cfg(any(feature = "v1_1", feature = "dox"))]
    #[cfg_attr(feature = "dox", doc(cfg(feature = "v1_1")))]
    #[doc(alias = "flatpak_installation_launch_full")]
    fn launch_full(
        &self,
        flags: LaunchFlags,
        name: &str,
        arch: Option<&str>,
        branch: Option<&str>,
        commit: Option<&str>,
        cancellable: Option<&impl IsA<gio::Cancellable>>,
    ) -> Result<Instance, glib::Error>;
}

impl<O: IsA<Installation>> InstallationExtManual for O {
    fn update_appstream_sync(
        &self,
        remote_name: &str,
        arch: Option<&str>,
        cancellable: Option<&impl IsA<gio::Cancellable>>,
    ) -> Result<(), glib::Error> {
        unsafe {
            let mut error = ptr::null_mut();
            let mut out_changed: glib::ffi::gboolean = 0;
            let _ = ffi::flatpak_installation_update_appstream_sync(
                self.as_ref().to_glib_none().0,
                remote_name.to_glib_none().0,
                arch.to_glib_none().0,
                &mut out_changed,
                cancellable.map(|p| p.as_ref()).to_glib_none().0,
                &mut error,
            );
            if error.is_null() {
                Ok(())
            } else {
                Err(from_glib_full(error))
            }
        }
    }

    fn update_appstream_full_sync(
        &self,
        remote_name: &str,
        arch: Option<&str>,
        progress: Option<&mut dyn (FnMut(&str, u32, bool))>,
        cancellable: Option<&impl IsA<gio::Cancellable>>,
    ) -> Result<(), glib::Error> {
        let progress_data: Option<&mut dyn (FnMut(&str, u32, bool))> = progress;
        unsafe extern "C" fn progress_func(
            status: *const libc::c_char,
            progress: libc::c_uint,
            estimating: glib::ffi::gboolean,
            user_data: glib::ffi::gpointer,
        ) {
            let status: Borrowed<glib::GString> = from_glib_borrow(status);
            let estimating = from_glib(estimating);
            let callback: *mut Option<&mut dyn (FnMut(&str, u32, bool))> =
                user_data as *const _ as usize as *mut Option<&mut dyn (FnMut(&str, u32, bool))>;
            if let Some(ref mut callback) = *callback {
                callback(status.as_str(), progress, estimating)
            } else {
                panic!("cannot get closure...")
            };
        }
        let progress = if progress_data.is_some() {
            Some(progress_func as _)
        } else {
            None
        };
        let super_callback0: &Option<&mut dyn (FnMut(&str, u32, bool))> = &progress_data;
        unsafe {
            let mut out_changed: glib::ffi::gboolean = 0;
            let mut error = ptr::null_mut();
            let _ = ffi::flatpak_installation_update_appstream_full_sync(
                self.as_ref().to_glib_none().0,
                remote_name.to_glib_none().0,
                arch.to_glib_none().0,
                progress,
                super_callback0 as *const _ as usize as *mut _,
                &mut out_changed,
                cancellable.map(|p| p.as_ref()).to_glib_none().0,
                &mut error,
            );
            if error.is_null() {
                Ok(())
            } else {
                Err(from_glib_full(error))
            }
        }
    }

    fn update_full(
        &self,
        flags: UpdateFlags,
        kind: RefKind,
        name: &str,
        arch: Option<&str>,
        branch: Option<&str>,
        subpaths: &[&str],
        progress: Option<&mut dyn (FnMut(&str, u32, bool))>,
        cancellable: Option<&impl IsA<gio::Cancellable>>,
    ) -> Result<InstalledRef, glib::Error> {
        let progress_data: Option<&mut dyn (FnMut(&str, u32, bool))> = progress;
        unsafe extern "C" fn progress_func(
            status: *const libc::c_char,
            progress: libc::c_uint,
            estimating: glib::ffi::gboolean,
            user_data: glib::ffi::gpointer,
        ) {
            let status: Borrowed<glib::GString> = from_glib_borrow(status);
            let estimating = from_glib(estimating);
            let callback: *mut Option<&mut dyn (FnMut(&str, u32, bool))> =
                user_data as *const _ as usize as *mut Option<&mut dyn (FnMut(&str, u32, bool))>;
            if let Some(ref mut callback) = *callback {
                callback(status.as_str(), progress, estimating)
            } else {
                panic!("cannot get closure...")
            };
        }
        let progress = if progress_data.is_some() {
            Some(progress_func as _)
        } else {
            None
        };
        let super_callback0: &Option<&mut dyn (FnMut(&str, u32, bool))> = &progress_data;
        unsafe {
            let mut error = ptr::null_mut();
            let ret = ffi::flatpak_installation_update_full(
                self.as_ref().to_glib_none().0,
                flags.into_glib(),
                kind.into_glib(),
                name.to_glib_none().0,
                arch.to_glib_none().0,
                branch.to_glib_none().0,
                subpaths.to_glib_none().0,
                progress,
                super_callback0 as *const _ as usize as *mut _,
                cancellable.map(|p| p.as_ref()).to_glib_none().0,
                &mut error,
            );
            if error.is_null() {
                Ok(from_glib_full(ret))
            } else {
                Err(from_glib_full(error))
            }
        }
    }

    fn uninstall(
        &self,
        kind: RefKind,
        name: &str,
        arch: Option<&str>,
        branch: Option<&str>,
        progress: Option<&mut dyn (FnMut(&str, u32, bool))>,
        cancellable: Option<&impl IsA<gio::Cancellable>>,
    ) -> Result<(), glib::Error> {
        let progress_data: Option<&mut dyn (FnMut(&str, u32, bool))> = progress;
        unsafe extern "C" fn progress_func(
            status: *const libc::c_char,
            progress: libc::c_uint,
            estimating: glib::ffi::gboolean,
            user_data: glib::ffi::gpointer,
        ) {
            let status: Borrowed<glib::GString> = from_glib_borrow(status);
            let estimating = from_glib(estimating);
            let callback: *mut Option<&mut dyn (FnMut(&str, u32, bool))> =
                user_data as *const _ as usize as *mut Option<&mut dyn (FnMut(&str, u32, bool))>;
            if let Some(ref mut callback) = *callback {
                callback(status.as_str(), progress, estimating)
            } else {
                panic!("cannot get closure...")
            };
        }
        let progress = if progress_data.is_some() {
            Some(progress_func as _)
        } else {
            None
        };
        let super_callback0: &Option<&mut dyn (FnMut(&str, u32, bool))> = &progress_data;
        unsafe {
            let mut error = ptr::null_mut();
            let _ = ffi::flatpak_installation_uninstall(
                self.as_ref().to_glib_none().0,
                kind.into_glib(),
                name.to_glib_none().0,
                arch.to_glib_none().0,
                branch.to_glib_none().0,
                progress,
                super_callback0 as *const _ as usize as *mut _,
                cancellable.map(|p| p.as_ref()).to_glib_none().0,
                &mut error,
            );
            if error.is_null() {
                Ok(())
            } else {
                Err(from_glib_full(error))
            }
        }
    }

    fn install_full(
        &self,
        flags: InstallFlags,
        remote_name: &str,
        kind: RefKind,
        name: &str,
        arch: Option<&str>,
        branch: Option<&str>,
        subpaths: &[&str],
        progress: Option<&mut dyn (FnMut(&str, u32, bool))>,
        cancellable: Option<&impl IsA<gio::Cancellable>>,
    ) -> Result<InstalledRef, glib::Error> {
        let progress_data: Option<&mut dyn (FnMut(&str, u32, bool))> = progress;
        unsafe extern "C" fn progress_func(
            status: *const libc::c_char,
            progress: libc::c_uint,
            estimating: glib::ffi::gboolean,
            user_data: glib::ffi::gpointer,
        ) {
            let status: Borrowed<glib::GString> = from_glib_borrow(status);
            let estimating = from_glib(estimating);
            let callback: *mut Option<&mut dyn (FnMut(&str, u32, bool))> =
                user_data as *const _ as usize as *mut Option<&mut dyn (FnMut(&str, u32, bool))>;
            if let Some(ref mut callback) = *callback {
                callback(status.as_str(), progress, estimating)
            } else {
                panic!("cannot get closure...")
            };
        }
        let progress = if progress_data.is_some() {
            Some(progress_func as _)
        } else {
            None
        };
        let super_callback0: &Option<&mut dyn (FnMut(&str, u32, bool))> = &progress_data;
        unsafe {
            let mut error = ptr::null_mut();
            let ret = ffi::flatpak_installation_install_full(
                self.as_ref().to_glib_none().0,
                flags.into_glib(),
                remote_name.to_glib_none().0,
                kind.into_glib(),
                name.to_glib_none().0,
                arch.to_glib_none().0,
                branch.to_glib_none().0,
                subpaths.to_glib_none().0,
                progress,
                super_callback0 as *const _ as usize as *mut _,
                cancellable.map(|p| p.as_ref()).to_glib_none().0,
                &mut error,
            );
            if error.is_null() {
                Ok(from_glib_full(ret))
            } else {
                Err(from_glib_full(error))
            }
        }
    }

    fn install(
        &self,
        remote_name: &str,
        kind: RefKind,
        name: &str,
        arch: Option<&str>,
        branch: Option<&str>,
        progress: Option<&mut dyn (FnMut(&str, u32, bool))>,
        cancellable: Option<&impl IsA<gio::Cancellable>>,
    ) -> Result<InstalledRef, glib::Error> {
        let progress_data: Option<&mut dyn (FnMut(&str, u32, bool))> = progress;
        unsafe extern "C" fn progress_func(
            status: *const libc::c_char,
            progress: libc::c_uint,
            estimating: glib::ffi::gboolean,
            user_data: glib::ffi::gpointer,
        ) {
            let status: Borrowed<glib::GString> = from_glib_borrow(status);
            let estimating = from_glib(estimating);
            let callback: *mut Option<&mut dyn (FnMut(&str, u32, bool))> =
                user_data as *const _ as usize as *mut Option<&mut dyn (FnMut(&str, u32, bool))>;
            if let Some(ref mut callback) = *callback {
                callback(status.as_str(), progress, estimating)
            } else {
                panic!("cannot get closure...")
            };
        }
        let progress = if progress_data.is_some() {
            Some(progress_func as _)
        } else {
            None
        };
        let super_callback0: &Option<&mut dyn (FnMut(&str, u32, bool))> = &progress_data;
        unsafe {
            let mut error = ptr::null_mut();
            let ret = ffi::flatpak_installation_install(
                self.as_ref().to_glib_none().0,
                remote_name.to_glib_none().0,
                kind.into_glib(),
                name.to_glib_none().0,
                arch.to_glib_none().0,
                branch.to_glib_none().0,
                progress,
                super_callback0 as *const _ as usize as *mut _,
                cancellable.map(|p| p.as_ref()).to_glib_none().0,
                &mut error,
            );
            if error.is_null() {
                Ok(from_glib_full(ret))
            } else {
                Err(from_glib_full(error))
            }
        }
    }

    fn install_bundle(
        &self,
        file: &impl IsA<gio::File>,
        progress: Option<&mut dyn (FnMut(&str, u32, bool))>,
        cancellable: Option<&impl IsA<gio::Cancellable>>,
    ) -> Result<InstalledRef, glib::Error> {
        let progress_data: Option<&mut dyn (FnMut(&str, u32, bool))> = progress;
        unsafe extern "C" fn progress_func(
            status: *const libc::c_char,
            progress: libc::c_uint,
            estimating: glib::ffi::gboolean,
            user_data: glib::ffi::gpointer,
        ) {
            let status: Borrowed<glib::GString> = from_glib_borrow(status);
            let estimating = from_glib(estimating);
            let callback: *mut Option<&mut dyn (FnMut(&str, u32, bool))> =
                user_data as *const _ as usize as *mut Option<&mut dyn (FnMut(&str, u32, bool))>;
            if let Some(ref mut callback) = *callback {
                callback(status.as_str(), progress, estimating)
            } else {
                panic!("cannot get closure...")
            };
        }
        let progress = if progress_data.is_some() {
            Some(progress_func as _)
        } else {
            None
        };
        let super_callback0: &Option<&mut dyn (FnMut(&str, u32, bool))> = &progress_data;
        unsafe {
            let mut error = ptr::null_mut();
            let ret = ffi::flatpak_installation_install_bundle(
                self.as_ref().to_glib_none().0,
                file.as_ref().to_glib_none().0,
                progress,
                super_callback0 as *const _ as usize as *mut _,
                cancellable.map(|p| p.as_ref()).to_glib_none().0,
                &mut error,
            );
            if error.is_null() {
                Ok(from_glib_full(ret))
            } else {
                Err(from_glib_full(error))
            }
        }
    }

    fn uninstall_full(
        &self,
        flags: UninstallFlags,
        kind: RefKind,
        name: &str,
        arch: Option<&str>,
        branch: Option<&str>,
        progress: Option<&mut dyn (FnMut(&str, u32, bool))>,
        cancellable: Option<&impl IsA<gio::Cancellable>>,
    ) -> Result<(), glib::Error> {
        let progress_data: Option<&mut dyn (FnMut(&str, u32, bool))> = progress;
        unsafe extern "C" fn progress_func(
            status: *const libc::c_char,
            progress: libc::c_uint,
            estimating: glib::ffi::gboolean,
            user_data: glib::ffi::gpointer,
        ) {
            let status: Borrowed<glib::GString> = from_glib_borrow(status);
            let estimating = from_glib(estimating);
            let callback: *mut Option<&mut dyn (FnMut(&str, u32, bool))> =
                user_data as *const _ as usize as *mut Option<&mut dyn (FnMut(&str, u32, bool))>;
            if let Some(ref mut callback) = *callback {
                callback(status.as_str(), progress, estimating)
            } else {
                panic!("cannot get closure...")
            };
        }
        let progress = if progress_data.is_some() {
            Some(progress_func as _)
        } else {
            None
        };
        let super_callback0: &Option<&mut dyn (FnMut(&str, u32, bool))> = &progress_data;
        unsafe {
            let mut error = ptr::null_mut();
            let _ = ffi::flatpak_installation_uninstall_full(
                self.as_ref().to_glib_none().0,
                flags.into_glib(),
                kind.into_glib(),
                name.to_glib_none().0,
                arch.to_glib_none().0,
                branch.to_glib_none().0,
                progress,
                super_callback0 as *const _ as usize as *mut _,
                cancellable.map(|p| p.as_ref()).to_glib_none().0,
                &mut error,
            );
            if error.is_null() {
                Ok(())
            } else {
                Err(from_glib_full(error))
            }
        }
    }

    fn update(
        &self,
        flags: UpdateFlags,
        kind: RefKind,
        name: &str,
        arch: Option<&str>,
        branch: Option<&str>,
        progress: Option<&mut dyn (FnMut(&str, u32, bool))>,
        cancellable: Option<&impl IsA<gio::Cancellable>>,
    ) -> Result<InstalledRef, glib::Error> {
        let progress_data: Option<&mut dyn (FnMut(&str, u32, bool))> = progress;
        unsafe extern "C" fn progress_func(
            status: *const libc::c_char,
            progress: libc::c_uint,
            estimating: glib::ffi::gboolean,
            user_data: glib::ffi::gpointer,
        ) {
            let status: Borrowed<glib::GString> = from_glib_borrow(status);
            let estimating = from_glib(estimating);
            let callback: *mut Option<&mut dyn (FnMut(&str, u32, bool))> =
                user_data as *const _ as usize as *mut Option<&mut dyn (FnMut(&str, u32, bool))>;
            if let Some(ref mut callback) = *callback {
                callback(status.as_str(), progress, estimating)
            } else {
                panic!("cannot get closure...")
            };
        }
        let progress = if progress_data.is_some() {
            Some(progress_func as _)
        } else {
            None
        };
        let super_callback0: &Option<&mut dyn (FnMut(&str, u32, bool))> = &progress_data;
        unsafe {
            let mut error = ptr::null_mut();
            let ret = ffi::flatpak_installation_update(
                self.as_ref().to_glib_none().0,
                flags.into_glib(),
                kind.into_glib(),
                name.to_glib_none().0,
                arch.to_glib_none().0,
                branch.to_glib_none().0,
                progress,
                super_callback0 as *const _ as usize as *mut _,
                cancellable.map(|p| p.as_ref()).to_glib_none().0,
                &mut error,
            );
            if error.is_null() {
                Ok(from_glib_full(ret))
            } else {
                Err(from_glib_full(error))
            }
        }
    }

    #[cfg(any(feature = "v1_1", feature = "dox"))]
    #[cfg_attr(feature = "dox", doc(cfg(feature = "v1_1")))]
    fn launch_full(
        &self,
        flags: LaunchFlags,
        name: &str,
        arch: Option<&str>,
        branch: Option<&str>,
        commit: Option<&str>,
        cancellable: Option<&impl IsA<gio::Cancellable>>,
    ) -> Result<Instance, glib::Error> {
        unsafe {
            let mut error = ptr::null_mut();
            let mut instance = ptr::null_mut();
            let _ = ffi::flatpak_installation_launch_full(
                self.as_ref().to_glib_none().0,
                flags.into_glib(),
                name.to_glib_none().0,
                arch.to_glib_none().0,
                branch.to_glib_none().0,
                commit.to_glib_none().0,
                &mut instance,
                cancellable.map(|p| p.as_ref()).to_glib_none().0,
                &mut error,
            );
            if error.is_null() {
                Ok(from_glib_none(instance))
            } else {
                Err(from_glib_full(error))
            }
        }
    }
}
